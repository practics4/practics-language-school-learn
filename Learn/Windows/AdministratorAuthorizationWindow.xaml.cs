﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Learn.Windows
{
    /// <summary>
    /// Логика взаимодействия для AdministratorAuthorizationWindow.xaml
    /// </summary>
    public partial class AdministratorAuthorizationWindow : Window
    {
        private const string ADMIN_CODE = "0000";
        public AdministratorAuthorizationWindow()
        {
            InitializeComponent();
        }

        private void confirm_Click(object sender, RoutedEventArgs e)
        {
            var passwordCode = password.Text;

            if (passwordCode == ADMIN_CODE)
            {
                DialogResult = true;
            } else
            {
                DialogResult = false;
            }
        }

        private void Button1_OnClick(object sender, RoutedEventArgs e)
        {
            password.Text += "1";
        }

        private void Button2_OnClick(object sender, RoutedEventArgs e)
        {
            password.Text += "2";
        }

        private void Button3_OnClick(object sender, RoutedEventArgs e)
        {
            password.Text += "3";
        }

        private void Button4_OnClick(object sender, RoutedEventArgs e)
        {
            password.Text += "4";
        }

        private void Button5_OnClick(object sender, RoutedEventArgs e)
        {
            password.Text += "5";
        }

        private void Button6_OnClick(object sender, RoutedEventArgs e)
        {
            password.Text += "6";
        }

        private void Button7_OnClick(object sender, RoutedEventArgs e)
        {
            password.Text += "7";
        }

        private void Button8_OnClick(object sender, RoutedEventArgs e)
        {
            password.Text += "8";
        }

        private void Button9_OnClick(object sender, RoutedEventArgs e)
        {
            password.Text += "9";
        }

        private void Button0_OnClick(object sender, RoutedEventArgs e)
        {
            password.Text += "0";
        }

        private void ButtonBackspace_OnClick(object sender, RoutedEventArgs e)
        {
            var passwordtext = password.Text;

            password.Text = password.Text.Substring(0, passwordtext.Length - 1);
        }
    }
}
